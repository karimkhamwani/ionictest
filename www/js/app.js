var app = angular.module('starter', ['ionic','starter.controllers','starter.services']);


app.config(function($stateProvider, $urlRouterProvider) {
  $stateProvider

  .state('home', {
        url: '/home',
        templateUrl: 'views/home.html',
        controller: 'HomeController',
         cache:false
      })
   .state('new', {
        url: '/new',
        templateUrl: 'views/new.html',
        controller: 'NewController',
         cache:false
      })

  $urlRouterProvider.otherwise('/home');
})
